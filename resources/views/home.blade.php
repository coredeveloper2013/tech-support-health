@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <style>
  *{
  box-sizing:border-box;
}
a{
  text-decoration:none;
}
ul#data-range-nav{
    list-style: none;
    margin: 0;
    padding: 10px 2rem;
}
ul#data-range-nav>li>a{
   padding:2rem;
}
ul#data-range-nav li {
   margin-bottom: 15px;
}
.show{
  display:block !important;
  transition: .35s ease-in-out;
}
.hide{
  display:none;
}
.date-range{
     position:relative;
}
.container{
    display:flex;
    justify-content:center;
    align-items:center;
}
#data-range-nav{
   background:#fff;
   position:absolute;
   /* min-width:800px; */
   min-height:350px;
   /* overflow:hidden; */
   left:25px;
   transition:display .35s ease-in-out;
   z-index:10;
   display: none;
   border: 2px solid #d4d4d4;
}
.date-dropdown{
    position:relative;
}
.date-range-select{
     display:grid;
     grid-template-columns:1fr 1fr;
     grid-gap:1.5rem;
}
.date-rage-class{
    position: absolute;
    min-width: 200px;
    left: 155px;
    min-height: 150px;
    top: -133px;
    display: none;
}

.new-data-range-nav{
   min-width:800px;
   overflow:hidden;
}
    </style>
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">

         <div class="panel panel-primary" style="margin-top:45px;">
            <div class="panel-heading">
                <strong>Agent Performance Report</strong>   <div class="date-range">
                        Date Range <a href="#" id="changerange" >Change</a>
                           <ul id="data-range-nav">
                           <li><a href="{{ url('home') }}?day=1">Yesterday</a></li>
                             <li><a href="{{ url('home') }}?day=7">Last 7 days</a></li>
                             <li><a href="{{ url('home') }}?day=30">Last 30 days</a></li>
                             <li><a href="{{ url('home') }}?day=90">Last 90 days</a></li>
                             <li class="date-dropdown"><a href="#" id="select-date-range">Select a date range</a>
                                   <div class="date-rage-class" id="date-range-wrapper">
                                         <div class="date-range-select">
                                            <div>
                                              <span>Start date</span>
                                              <input type="hidden" id="first" />
                                              <div id="datepicker1"></div>
                                           </div>
                                            <div>
                                              <span>End date</span>
                                              <input type="hidden" id="second" />
                                              <div id="datepicker2"></div>
                                           </div>
                                        </div>
                                        <button class="btn" onclick="sendDate()">Done</button>

                                   </div>
                             </li>
                           </ul>

                   </div> <br>
                {{-- <small><strong>( Agent Perfomance Data from {{ date_format(date_create($timespan['from']), "Y-M-d")  }} to {{ date_format(date_create($timespan['today']), "Y-M-d")  }} )</strong></small> --}}
            </div>
             <div class="panel-body">
                <div class="col-md-12">
                    <table class="table table-bordered myTable">
                      <thead>
                          <tr>

                              <th>Agent</th>
                              <th>Tickets (7 days)</th>
                              <th>Resolved (7 days)</th>
                              <th>Over Due (Any time)</th>
                              <th>FR SLA%</th>
                              <th>R SLA%</th>
                              <th>FCR%</th>
                              <th>Average 1st Response time</th>
                              <th>Average Response</th>
                              <th>Average Resolution</th>
                          </tr>
                      </thead>
                      <tbody>
                          @foreach ($agentData as $agdata)
                          <tr>
                          <td>{{ $agdata->agent_name  }}</td>
                                <td>{{ $agdata->total_tickets  }}</td>
                                <td>{{ $agdata->resolved }}</td>
                                <td>{{ $agdata->due_by  }}</td>

                                <td>{{ $agdata->fr_sla }}</td>
                                <td></td>
                                <td></td>
                                <td>{{ $agdata->average_first_response_time  }}</td>
                                <td></td>
                                <td></td>
                            </tr>
                          @endforeach

                      </tbody>
                    </table>
               </div>
             </div>
         </div>




    </div>



    <div class="row">

         <div class="panel panel-primary" style="margin-top:45px;">
            <div class="panel-heading">
                <strong>LogMe In</strong>
            </div>
             <div class="panel-body">
                <div class="col-md-12">
                    <table class="table table-bordered myTable" >
                      <thead>
                          <tr>
                              <th>Client</th>
                              <th>Importance Updates # (online)</th>
                              <th># of Alert < 90 days</th>
                              <th>Tboxes Offline</th>
                          </tr>
                      </thead>
                      <tbody>
                          @foreach ($logmeindata as $logme)
                          <tr>
                          <td>{{ $logme->name }} ( {{$logme->noofcoumputer}} coumputers  )</td>
                                <td>17</td>
                                <td>8</td>
                                <td>1 Since</td>
                            </tr>
                          @endforeach

                      </tbody>
                    </table>
               </div>
             </div>
         </div>




    </div>
</div>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
   <script>
    $(document).ready( function () {
       $('.myTable').DataTable();

       $('#datepicker1').datepicker({
            inline: true,
            altField: '#first'
        });
        $('#first').change(function(){
            $('#datepicker1').datepicker('setDate', $(this).val());
        });

        $('#datepicker2').datepicker({
            inline: true,
            altField: '#second'
        });
        $('#second').change(function(){
            $('#datepicker2').datepicker('setDate', $(this).val());
        });

        $("#changerange").click(function(){
            $("#data-range-nav").toggleClass("show");
        });
        $("#select-date-range").click(function(){
            $("#data-range-nav").toggleClass("new-data-range-nav");
            $("#date-range-wrapper").toggleClass("show");
        });
    });

    function sendDate() {
        var first = $("#first").val();
        var second = $("#second").val();
        if (first!='' && second!='') {
            window.location.href = "{{ url('home') }}?first="+first+"&second="+second;
        } else {
            alert('Date range not defined');
        }
    }
   </script>
@endsection
