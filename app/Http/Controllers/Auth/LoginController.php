<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    private function is_active()
    {
        return "is_active";
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
            $this->is_active() => ["Your account is not activated, contact with administration"],
        ]);
    }

    protected function attemptLogin(Request $request)
        {
            $credentials = $this->credentials($request);

            $credentials['is_active'] = 1; // Additional field you want to check

            return $this->guard()->attempt(
                $credentials, $request->filled('remember')
            );
        }

  /*   public function authenticate()
    {
         dd("hello world");
        if (Auth::attempt(['email' => $email, 'password' => $password, 'is_active' => 1])) {
            // Authentication passed...
            return redirect()->route('/home');
        }
        return redirect()->route('/login')->with('error','Your account is not activated');
    } */
}
