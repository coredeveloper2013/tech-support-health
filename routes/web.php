<?php
use function GuzzleHttp\json_decode;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

 Route::get('/testing', function () {

         function &paypal()
            {
                $obj = new stdClass;
                $obj->url = "sending url string";
                $obj->path = "definiation";
                return $obj;
            }

            function configfunc(&$func)
            {
                $obj = $func;
                $obj->hint = "hello world";
                echo $obj->hint."</br>";
                echo $obj->url."</br>";
                echo $obj->path."</br>";
            }

        configfunc(paypal());


});

/*
Route::get('/logmein', function () {

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://secure.logmein.com/public-api/v2/hostswithgroups",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Authorization:  Basic ".base64_encode(env('LOGMEIN_CLIENT_ID').':'.env('LOGMEIN_CLIENT_APP_KEY')),
            "content-type: application/json",
        ),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    echo "<pre>";
      print_r( json_decode($response) );
});

Route::get('/tic', function () {
    // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://thrivemes.freshdesk.com/api/v2/search/tickets?query="status:4"');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($ch, CURLOPT_USERPWD, 'tim.saddoris@thrivemes.com' . ':' . 'tS3GkTqEY29k');

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
        echo "<pre>";
       dd( json_decode( $result));
   // return view('welcome');
}); */


Route::get('/', function () {
    return view('welcome');
});




/* Route::get('/tickets', function () {
    // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://mediusware.freshdesk.com/api/v2/search/tickets?query="agent_id:48005981336%20AND%20status:4"');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($ch, CURLOPT_USERPWD, 'atik@mediusware.com' . ':' . 'feni@2019');

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
        echo "<pre>";
        dd($result);
       print_r( json_decode($result));
   // return view('welcome');
}); */

/*
Route::get('/agents', function () {
    // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://thrivemes.freshdesk.com/api/v2/agents');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($ch, CURLOPT_USERPWD, 'tim.saddoris@thrivemes.com' . ':' . 'tS3GkTqEY29k');

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
        echo "<pre>";
       // dd($result);
       print_r( json_decode($result));
   // return view('welcome');
});
Route::get('/dueby', function () {
    // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://thrivemes.freshdesk.com/api/v2/search/tickets?query="agent_id:5038664996%20AND%20(type:%27Question%27%20OR%20type:%27Problem%27)%20AND%20(due_by:>%272017-10-01%27%20AND%20due_by:<%272019-07-01%27)"');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($ch, CURLOPT_USERPWD, 'tim.saddoris@thrivemes.com' . ':' . 'tS3GkTqEY29k');

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
        echo "<pre>";
        //dd($result);
       print_r( json_decode($result));
   // return view('welcome');
});
 */

Route::get('/sla', function () {
    // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://thrivemes.freshdesk.com/api/v2/tickets/2465?include=stats');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($ch, CURLOPT_USERPWD, 'tim.saddoris@thrivemes.com' . ':' . 'tS3GkTqEY29k');

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
        echo "<pre>";
        //dd($result);
       print_r( json_decode($result));
   // return view('welcome');
});



Auth::routes();

Route::get('/home', 'HomeController@index');

Route::resource('users', 'UserController');
