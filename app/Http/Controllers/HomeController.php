<?php

namespace App\Http\Controllers;

use DateTime;
use stdClass;
use Illuminate\Http\Request;
use function GuzzleHttp\json_decode;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

     public function formatLogMeData($obj)
     {
         $responsedata = array();
         $responsedata = collect($obj->groups)->map(function($dd)  {
                 $myobj = new stdClass;
                 $myobj->group_id = $dd->id;
                 $myobj->noofcoumputer = 0;
                 $myobj->name = $dd->id == -1 ? "Default Group": $dd->name;
               return $myobj;
          });

          foreach($obj->hosts as $host) {

                   foreach($responsedata as $eachdata)
                   {
                       if($eachdata->group_id == $host->groupid) $eachdata->noofcoumputer++;
                   }
          }

     return $responsedata;
     }
     public function format_date($date){
       return date_format(date_create($date),'Y-m-d');
     }
    public function index(Request $request)
    {
       $day =  $request->day;
       $firstdate =  $request->first;
       $seconddate =  $request->second;

       $queryobj = new stdClass;
       $queryobj->start_date = date('Y-m-d',strtotime(date('Y-m-d').'-1 year'));
       $queryobj->end_date  = date('Y-m-d');

        if(!empty($day))
        {
            if((int)$day == 1){
                $queryobj->start_date = date('Y-m-d',strtotime(date('Y-m-d').'-'.(int)$day.' day'));
            }
            else {
                $queryobj->start_date = date('Y-m-d',strtotime(date('Y-m-d').'-'.(int)$day.' days'));
            }

        }

        if(!empty($firstdate) && !empty($seconddate))
        {
            $queryobj->start_date =   $this->format_date($firstdate);
            $queryobj->end_date   =   $this->format_date($seconddate);
        }

        $date_of_range = [
            "from" => date('Y-m-d',strtotime(date('Y-m-d').'-1 year')),
            "today" => date("Y-m-d")

        ];
        return view('home')
            ->with('agentData',$this->getFresDeskInfo($queryobj))
            ->with('timespan',$date_of_range)
            ->with('logmeindata',$this->getLogMeInData());
    }
    public function getLogMeInData()
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://secure.logmein.com/public-api/v2/hostswithgroups",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Authorization:  Basic ".base64_encode(env('LOGMEIN_CLIENT_ID').':'.env('LOGMEIN_CLIENT_APP_KEY')),
                "content-type: application/json",
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            return null;
        } else {
            return $this->formatLogMeData(json_decode($response));
        }
    }

    public function getFresDeskInfo($query)
    {

        $data = array();
        $this->curlRequest("https://thrivemes.freshdesk.com/api/v2/agents"
                      ,"GET"
                      ,function($agents) use (&$data,&$query)
                      {

                            foreach($agents as $agent)
                            {
                                        $obj = new stdClass;
                                        $obj->agent_id = $agent->id;
                                        $obj->agent_name = $agent->contact->name;






                                        $this->curlRequest(
                                            'https://thrivemes.freshdesk.com/api/v2/search/tickets?query="agent_id:'.$agent->id.'%20AND%20created_at:>%27'.$query->start_date.'%27"'
                                            ,"GET"
                                            ,function($res) use ($obj) {
                                            //    dd($res);
                                            //    foreach($res->results as $ticket)
                                            //        {
                                            //                     dump($ticket);

                                            //        }
                                                $obj->total_tickets = $res->total;
                                            });

                                            // dd($last_year);

                                            $this->curlRequest(
                                                'https://thrivemes.freshdesk.com/api/v2/search/tickets?query="agent_id:'.$agent->id.'%20AND%20updated_at:>%27'.$query->start_date.'%27%20AND%20updated_at:<%27'.$query->end_date.'%27%20AND%20(status:4%20OR%20status:5)"'
                                                ,"GET"
                                                ,function($res) use ($obj) {

                                                   $i = 0;
                                                   foreach($res->results as $ticket)
                                                   {
                                                             if($ticket->is_escalated == false) {
                                                                // dump($ticket);
                                                                $i++;
                                                             }
                                                   }
                                                     $obj->resolved = $i;
                                                   // dd($i);
                                                });



                                            //dd($yesterday);
                                        $this->curlRequest(
                                            'https://thrivemes.freshdesk.com/api/v2/search/tickets?query="agent_id:'.$agent->id.'%20AND%20due_by:<%27'.$query->start_date.'%27%20AND%20fr_due_by:<%27'.$query->end_date.'%27%20AND%20(status:2%20OR%20status:3%20OR%20status:6%20OR%20status:7)"'
                                            ,"GET"
                                            ,function($res) use ($obj) {
                                                //dd($res);
                                                $obj->due_by = $res->total;
                                                //dd($res->total);
                                            });



                                            $this->curlRequest(
                                                'https://thrivemes.freshdesk.com/api/v2/tickets?include=stats'
                                                ,"GET"
                                                ,function($res) use ($obj) {
                                                $counter = 0;
                                                $total_time = 0;
                                                // dump('ATIK HERE');

                                                foreach($res as $ticket)
                                                {
                                                    //dump($ticket->responder_id.' '. $ticket->created_at.' '. $ticket->stats->first_responded_at);
                                                    if($ticket->stats->first_responded_at != null && $ticket->responder_id == $obj->agent_id){
                                                            $total_time += strtotime($ticket->stats->first_responded_at) - strtotime($ticket->created_at);
                                                            $counter++;
                                                    }


                                                }
                                                $obj->fr_sla  = '-';

                                                // dd($total_time. " ". $i);
                                                // $counter = 19;
                                                $obj->average_first_response_time  = '-';

                                                if($counter){
                                                    $first_SLA = ($counter*100)/count($res);
                                                    $obj->fr_sla = (int)$first_SLA."%";

                                                    $average = $total_time / $counter;

                                                    $dayy =  $average / (24 * 3600);

                                                    $average = $average % (24 * 3600);

                                                    $hour = $average / 3600;

                                                    $average %= 3600;
                                                    $minutes = $average / 60 ;

                                                    $average %= 60;
                                                    $seconds = $average;

                                                    $first_response = "";

                                                    if((int)$dayy)  $first_response .= (int)$dayy.'d ';
                                                    if((int)$hour)  $first_response .= (int)$hour.'h ';
                                                    if((int)$minutes)  $first_response .= (int)$minutes.'m ';


                                                    $obj->average_first_response_time  = $first_response;

                                                }


                                                    // $obj->total_tickets = $res->total;
                                                });



                                        array_push($data,$obj);
                            }
                    });
       return empty($data)?null:$data;
    }

   public function curlRequest( string  $url, string $requesttype, callable $myfunciton)
   {
    //    dd($url);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $requesttype);
            curl_setopt($ch, CURLOPT_USERPWD, 'tim.saddoris@thrivemes.com' . ':' . 'tS3GkTqEY29k');
            $tokenslists = curl_exec($ch);
            if (curl_errno($ch)) {
                echo 'Error:' . curl_error($ch);
            }
            curl_close($ch);
            $myfunciton(json_decode($tokenslists));
   }
}
